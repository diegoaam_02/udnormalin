var DrawerGraph = {};

$(function() {

	DrawerGraph =  {

		nodes:[], edges:[], network:[],
		attributes : [],
		functionalDependencies : [],
		data:{},

		/**
		 *
		 */
		addNode :function addNode() {
			try {
				var node = $('#node-label');
				var nodeName = $(node).val().toUpperCase();
				this.addAttribute(this.nodes, nodeName);
				this.data.attributes = this.attributes;
				sessionStorage.setItem("globalRelation", JSON.stringify(this.data, null, 4));
				$('#nodes').html(JSON.stringify(this.attributes, null, 4));
				$(node).val("");
			}
			catch (err) {
				alert(err);
			}
		},


		/**
		 * Agrega un atributo al conjunto universo y a los nodos del grafo
		 * @param nodes
		 * @param nodeName
		 */

		addAttribute : function (nodes, nodeName) {
			ValidationUtils.validateAttribute(nodes._data, nodeName);
			this.nodes.add({
				id: nodeName,
				label: nodeName
			});
			this.attributes.push(nodeName);
		},

		/***
		 * Dibujar el grafo del modelo a  partir de un arhivo json
		 * @param jsonFile
		 */
		drawJSON: function(jsonFile) {
			try {
				if (jsonFile.attributes) {
					//Crear nodos
					this.nodes = new vis.DataSet();
					this.functionalDependencies = [];
					this.attributes = [];
					for (var index in jsonFile.attributes) {
						this.addAttribute(this.nodes, jsonFile.attributes[index]);
					}

					if (jsonFile.functionalDependencies) {
						this.edges = new vis.DataSet();
						for (var index in jsonFile.functionalDependencies) {
							this.addDependencie(this.edges, jsonFile.functionalDependencies[index].descriptor, jsonFile.functionalDependencies[index].implied);
						}
						this.data.attributes = this.attributes;
						this.data.functionalDependencies = this.functionalDependencies;
						sessionStorage.setItem("globalRelation", JSON.stringify(this.data, null, 4));
						$('#edges').html(JSON.stringify(this.functionalDependencies, null, 4));
						$('#nodes').html(JSON.stringify(this.attributes, null, 4));
						this.draw(this.nodes, this.edges);
					}

				} else {

					throw new Error('No hay elementos');
				}
				//});
			} catch (err) {
				this.nodes = new vis.DataSet();
				this.edges = new vis.DataSet();
				this.functionalDependencies = [];
				this.attributes = [];
				$('#edges').html(JSON.stringify(this.functionalDependencies, null, 4));
				$('#nodes').html(JSON.stringify(this.attributes, null, 4));
				alert(err);
			}
		},

		/**
		 * Add graph
		 */
		addEdge : function () {
			try {
				var nodeFrom = $('#edge-from');
				var from = $(nodeFrom).val().toUpperCase();
				var fromArray = from.split(",").sort();
				from = fromArray.toString();
				var descriptor = [];
				//Descriptor
				if (fromArray.length > 1) {
					//Seudo nodo
					var exist = this.nodes._data[from];
					if (!exist) {
						this.nodes.add({
							id: from,
							label: from
						});
					}

					for (var x in fromArray) {
						if (!exist) {
							this.edges.add({
								id: from + '-' + fromArray[x],
								from: fromArray[x],
								to: from,
								arrows: 'to',
								dashes: true
							});
						}
						descriptor.push(fromArray[x]);
					}

				} else {
					descriptor.push(from);

				}
				//Implied
				var nodeTo = $('#edge-to');
				var to = $(nodeTo).val().toUpperCase();
				var toArray = to.split(",");
				ValidationUtils.validateDependencies(this.nodes._data,fromArray,this.edges._data,toArray);
				for(x in toArray){
					var dependencie = {};
					dependencie.descriptor = descriptor;
					dependencie.implied = [];
					var toTemp = toArray[x];
					this.edges.add({
						id: from + '-' + toTemp,
						from: from,
						to: toTemp,
						arrows: 'to'
					});
					dependencie.implied.push(toTemp);
					this.functionalDependencies.push(dependencie);
				}
				this.data.functionalDependencies = this.functionalDependencies;
				sessionStorage.setItem("globalRelation", JSON.stringify(this.data, null, 4));
				$('#edges').html(this.toJSON(this.functionalDependencies));
				nodeTo.val("");
				nodeFrom.val("");
			} catch (err) {
				alert(err);
			}
		},

		addDependencie: function (edges, descriptors, implieds) {

			var from = descriptors.toString().toUpperCase();
			var fromArray = from.split(",").sort();
			from = fromArray.toString();
			var descriptor = [];
			//Descriptor
			if (fromArray.length > 1) {
				//Seudo nodo
				var exist = this.nodes._data[from];
				if (!exist) {
					this.nodes.add({
						id: from,
						label: from
					});
				}
				for (var x in fromArray) {
					if (!exist) {
						this.edges.add({
							id: from + '-' + fromArray[x],
							from: fromArray[x],
							to: from,
							arrows: 'to',
							dashes: true
						});
					}
					descriptor.push(fromArray[x]);
				}
			} else {
				descriptor.push(from);
			}

			//Implied
			var to = implieds.toString().toUpperCase();
			var toArray = implieds;
			ValidationUtils.validateDependencies(this.nodes._data,fromArray,this.edges._data,toArray);
			for(x in toArray) {
				var dependencie = {};
				dependencie.descriptor = descriptor;
				dependencie.implied = [];
				var toTemp = toArray[x];
				this.edges.add({
					id: from + '-' + toTemp,
					from: from,
					to: toTemp,
					arrows: 'to'
				});
				dependencie.implied.push(toTemp);
				this.functionalDependencies.push(dependencie);
			}
		},


		// convenience method to stringify a JSON object
		toJSON:function(obj) {
			return JSON.stringify(obj, null, 4);
		},

		 draw:function(nodes,edges) {
			// create an array with nodes

			var container = document.getElementById('network');
			var data = {
				nodes: nodes,
				edges: edges
			};
			var options = {};
			network = new vis.Network(container, data, options);

		}

	};

	DrawerGraph.nodes = new vis.DataSet();
	DrawerGraph.edges = new vis.DataSet();
	DrawerGraph.draw(DrawerGraph.nodes, DrawerGraph.edges);
});
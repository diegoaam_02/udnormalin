/**
 * Created by diegoamaya on 26/09/15.
 */

/**
 * ************************************************************************
 * Utils for Functional Dependencies
 * ************************************************************************
 */
var FunctionalDependenciesUtils = {};
/**
 * ************************************************************************
 * Utils for Combinations
 * ************************************************************************
 */
var CombinationUtils = {};
/**
 * ************************************************************************
 * Utils for General purposes
 * ************************************************************************
 */
var GeneralUtils = {};

/**
 * ************************************************************************
 * Validation Utils
 * ************************************************************************
 */
var ValidationUtils = {};

$(function(){

    /**
     * ************************************************************************
     * Utils for Functional Dependencies
     * ************************************************************************
     */
    FunctionalDependenciesUtils = {
        /**
         * Retorna la longitud maxima de elementos que contienen los descriptores de un
         * conjunto de dependencias funcionales
         * @param functionalDependencies conjunto de dependencias funcionales
         * @returns {number} numero mayor de atributos que tienen entre todos los descriptores
         */
        getMaxSizeFromDescriptors : function (functionalDependencies){
            var max = 0;
            for(var i in functionalDependencies){
                if(functionalDependencies[i].descriptor.length > max){
                    max = functionalDependencies[i].descriptor.length;
                }
            }
            return max;
        },

        /**
         * Busca que implicados tiene un descriptor
         * @param {ArrayOfDependencies} functionalDependencies dependencias funcionales a ser usadas
         * @param {Array} descriptor arreglo normal de descriptor a calcular cierre
         * @returns {ArrayOfArrays} arreglo de arreglos con los implicados de un descriptor dado
         */
        searchForImpliedGroup: function(functionalDependencies, descriptor){
            //Array of Arrays
            var impliedGroup = [];
            for(var i in functionalDependencies){
                var localDesc = functionalDependencies[i].descriptor;
                if(GeneralUtils.areArraysEquals(localDesc,descriptor)){
                    impliedGroup.push(functionalDependencies[i].implied);
                }
            }
            return impliedGroup;
        },


        /**
         * Devuelve el conjunto de implicados y descripores del conjunto de dependencias funcionales
         * @param {ArrayOfDependencies}  functionalDependencies del obtenidas del algoritmo de recubrimiento minimo
         * @returns {{descriptors: Array, implied: Array}}
         */
        searchDescriptorsImplieds: function(functionalDependencies){
            var descriptors = [];
            var descriptorsObject = {};
            var implied = [];
            var impliedObject = {};
            for(var i in functionalDependencies) {
                var functionalDependencie = functionalDependencies[i];

                for (var x in functionalDependencie.descriptor) {
                    var attribute = functionalDependencie.descriptor[x];
                    if (!descriptorsObject[attribute]) {
                        descriptorsObject[attribute] = attribute;
                        descriptors.push(attribute);
                    }
                }
                for (var x in functionalDependencie.implied) {
                    var attribute = functionalDependencie.implied[x];
                    if (!impliedObject[attribute]) {
                        impliedObject[attribute] = attribute;
                        implied.push(attribute);
                    }
                }
            }
                return{descriptors:descriptors,implied:implied};
        }

        };

    /**
     * ************************************************************************
     * Utils for Combinations
     * ************************************************************************
     */
    CombinationUtils = {

        /**
         * Se realizan las combinaciones posibles de un conjunto de elementos de un arreglo normal
         * @param descriptorsArray arreglo normal con cada uno de los atributos a combinar
         * @param maxNumberOfCombination arreglo de arreglo con las combinaciones posibles (descriptores)
         */
        findCombinations : function(descriptorsArray, maxNumberOfCombination) {
            //Todo: Mejorar rendimiento
            var fn = function(active, rest, a) {
                if ((active == null || active.length == 0) && (rest == null || rest.length == 0))
                    return;
                if (rest == null || rest.length == 0) {
                    a.push(active);
                } else {
                    var activeCopy = active.slice();
                    activeCopy.push(rest[0])
                    fn(activeCopy, rest.slice(1), a);
                    fn(active, rest.slice(1), a);
                }
                return a;
            }
            var result = fn([], descriptorsArray, []);
            for(var i = 0; i< result.length; i = i+1){
                if(result[i].length > maxNumberOfCombination){
                    result.splice(i, 1);
                    i--;
                }
            }
            return result;
        },
        /**
         * Extrae las combinaciones que aun no han sido usadas del conjunto total
         * @param allCombinations array of arrays todas las convinaciones posibles
         * @param usedCombination array of arrays combinaciones que fueron usadas actualmente
         * @returns {ArrayOfArray} arreglo de arreglos con las combinaciones aun no usadas
         */
        extractNotUsedCombination : function (allCombinations, usedCombination){
            var diff = allCombinations.filter(function(x) {
                if(usedCombination != null && usedCombination.length > 0){
                    var result = false;
                    for(var i = 0; i < usedCombination.length && result == false; i++){
                        result = GeneralUtils.areArraysEquals(usedCombination[i], x)
                    }
                    return !result;
                }else{
                    return true;
                }
            })
            return diff;
        }
    },

    /**
     * ************************************************************************
     * General Utils
     * ************************************************************************
     */
    GeneralUtils = {

        /**
         * Validates if 2 arrays are equals
         * @param array1 array to be compared with the other
         * @param array2 array to be compared with the other
         * @returns {boolean} true if they are equals
         */
        areArraysEquals: function (array1, array2){

            // if the other array is a falsy value, return
            if (!array2)
                return false;

            // compare lengths - can save a lot of time
            if (array1.length != array2.length)
                return false;

            for (var i = 0, l=array1.length; i < l; i++) {
                // Check if we have nested arrays
                if (array1[i] instanceof Array && array2[i] instanceof Array) {
                    // recurse into the nested arrays
                    if (!array1[i].equals(array2[i]))
                        return false;
                }
                else if (array1[i] != array2[i]) {
                    // Warning - two different object instances will never be equal: {x:20} != {x:20}
                    return false;
                }
            }
            return true;

        },
        /**
         * Algoritmo de busqueda para encontrar un elemento en un arreglo.
         * @param items
         * @param value
         * @returns {number}
         */
         binarySearch:function(items, value){

            var startIndex  = 0,
                stopIndex   = items.length - 1,
                middle      = Math.floor((stopIndex + startIndex)/2);

            while(items[middle] != value && startIndex < stopIndex){

                //adjust search area
                if (value < items[middle]){
                    stopIndex = middle - 1;
                } else if (value > items[middle]){
                    startIndex = middle + 1;
                }

                //recalculate middle
                middle = Math.floor((stopIndex + startIndex)/2);
            }

            //make sure it's the right value
            return (items[middle] != value) ? -1 : middle;
        },


        /**
         * Obtiene la diferencia de dos arreglos
         * @param array1
         * @param array2
         * @returns {Array} arreglo con los elementos distintos de los arreglos de entrada
         */
        arrayDifference:function(array1, array2)
        {
            var a=[], diff=[];
            for(var i=0;i<array1.length;i++)
                a[array1[i]]=true;
            for(var i=0;i<array2.length;i++)
                if(a[array2[i]]) delete a[array2[i]];
                else a[array2[i]]=true;
            for(var k in a)
                diff.push(k);
            return diff;
        }
    },

    /**
     * ************************************************************************
     * Validation Utils
     * ************************************************************************
     */
    ValidationUtils = {
        /**
         * Validates the attributes in the relation that means special character
         * and if the attribute is already in the list
         * @param listOfAttributes
         * @param currentAttribute
         */
        validateAttribute: function (listOfAttributes, currentAttribute) {

            if(!currentAttribute.match(/^[A-Z0-9]+\w*$/g)){
                throw new Error('The attribute '+currentAttribute+'  has invalid caracters');
            }
            if(listOfAttributes[currentAttribute]){
                throw new Error('The attribute '+currentAttribute+' is already in the relation');
            }
        },


        /**
         * Validates dependencies
         * @param attributes
         * @param descriptors
         * @param dependencie
         * @param implieds
         */
        validateDependencies: function(attributes, descriptors, dependencie, implieds) {
            var invalidateElemet = "";
            var descriptorsString = descriptors.toString();
            //Validar que los atributos existan en el conjunto
            for(var x in descriptors){
                //Buscar los que no estan en conjunto de atributos
                if(!attributes[descriptors[x]]){
                    invalidateElemet +=","+descriptors[x];
                }
            }
            for(var x in implieds){
                var implied = implieds[x];
                //Buscar los que no estan en conjunto de atributos
                if(!attributes[implied]){
                    invalidateElemet +=","+implied;
                }

                if(dependencie[descriptorsString+"-"+implied]){
                    throw new Error('La dependencia funcional '+descriptorsString+' implica '+implied+' ya existe');
                }
                // Busca dependencias funciones triviales
                debugger;
                if(GeneralUtils.binarySearch(descriptors,implied)!=-1){
                    throw new Error('La dependencia funcional '+descriptorsString+' implica '+implied+' es trivial');
                }

            }

            if(invalidateElemet){
                throw new Error('Los atributos '+invalidateElemet.substr(1)+' no existen en el conjunto de elementos');
            }
        }

    }

})
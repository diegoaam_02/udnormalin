/**
 * Created by diegoamaya on 27/09/15.
 */

/**
 * ************************************************************************
 * Import JSON Utils
 * ************************************************************************
 */
var ImportJSONUtil = {};
/**
 * ************************************************************************
 * Export JSON Utils
 * ************************************************************************
 */
var ExportJSONUtil = {};

/**
 * ************************************************************************
 * Coating Iterations Utils
 * ************************************************************************
 */
var CoatingIterationsUtils = {};

/**
 * ************************************************************************
 * Generate ER Utils
 * ************************************************************************
 */
var GenerateERUtils = {};


$(function(){
    /**
     * Export Utils
     * @type {{exportCurrentJSON: Function}}
     */
    ExportJSONUtil = {
        /**
         *
         */
        exportCurrentJSON : function(){
            if(typeof(Storage) !== "undefined") {
                if(sessionStorage.getItem("globalRelation") != null){
                    var globalRelation = sessionStorage.getItem("globalRelation");
                    // Set objects for file generation.
                    var blob, url, a;

                    // Set MIME type and encoding.
                    var fileType = "application/json;charset=utf-8;";
                    var fileName = "globalRelation.json";

                    // Set data on blob.
                    blob = new Blob( [ globalRelation ], { type: fileType } );
                    // Set view.
                    if ( blob ) {
                        // Read blob.
                        url = window.URL.createObjectURL( blob );
                        // Create link.
                        a = document.createElement( "a" );
                        // Set link on DOM.
                        document.body.appendChild( a );
                        // Set link's visibility.
                        a.style = "display: none";
                        // Set href on link.
                        a.href = url;
                        // Set file name on link.
                        a.download = fileName;
                        // Trigger click of link.
                        a.click();
                        // Clear.
                        window.URL.revokeObjectURL( url );
                    } else {
                        // Handle error.
                    }
                }else{
                    alert("Please create a new diagram or import it from a JSON file");
                }
            } else {
                alert("Please download a current browser, make part of the new wave!");
            }
        }

    },

    /**
     *
     * @type {{readFile: Function, importJSON: Function, handleFileSelect: Function, handleFileSelectSelector: Function, handleDragOver: Function}}
     */
        ImportJSONUtil = {

            /**
             * It reads the JSON FILE
             * @param files
             */
            readFile : function(files){
                // Loop through the FileList and render image files as thumbnails.
                for (var i = 0, f; f = files[i]; i++) {
                    var reader = new FileReader();
                    // Closure to capture the file information.
                    reader.onload = (function(theFile) {
                        return function(e) {
                            $.getJSON(e.target.result, function( data ) {

                                if(data != null){
                                    if(typeof(Storage) !== "undefined") {
                                        //validar formato JSON
                                        try{
                                            var r = confirm("Are you sure you want to import a new JSON, this action will override your current work");
                                            if (r == true) {
                                                sessionStorage.setItem("globalRelation", JSON.stringify(data, null, 4));
                                                DrawerGraph.drawJSON(data);
                                            }
                                            $('#importJSONModal').modal('toggle');
                                        }catch(err){
                                            alert(err);
                                        }
                                    } else {
                                        alert("Pleas download a current browser, make part of the new wave!");
                                    }
                                }else{
                                    alert('Please make sure that your file is a JSON and has some data');
                                }
                            }).fail(function() {
                                alert('Please make sure that your file is a JSON and has some data');
                            });
                        };
                    })(f);
                    // Read the JSON as a data on URL(it encodes in base64)
                    reader.readAsDataURL(f);
                }
            },

            /**
             *
             * Todo: Review where is the correct place for this method
             */
            importJSON : function(){
                $('#JSONFileSelector').val('');
                if (window.File && window.FileReader && window.FileList && window.Blob) {
                    $('#importJSONModal').modal('toggle')
                } else {
                    alert('The File APIs are not fully supported in this browser.');
                }
            },

            /**
             *
             * @param evt
             */
            handleFileSelect: function (evt) {
                evt.stopPropagation();
                evt.preventDefault();

                var files = evt.dataTransfer.files; // FileList object.

                // files is a FileList of File objects. List some properties.
                ImportJSONUtil.readFile(files)
            },

            /**
             *
             * @param evt
             */
            handleFileSelectSelector: function(evt) {
                var files = evt.target.files; // FileList object
                ImportJSONUtil.readFile(files)
            },

            /**
             * Handle for drag over on the container
             * @param evt event
             */
            handleDragOver: function(evt) {
                evt.stopPropagation();
                evt.preventDefault();
                evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
            }
        },

        /**
         * ************************************************************************
         * Coating Iterations Utils
         * ************************************************************************
         */
        CoatingIterationsUtils = {
            updateIterations : function(){
                if(sessionStorage.getItem("globalRelation") != null){
                    if(typeof(Storage) !== "undefined") {
                        try{
                            var isCoatingRunning = sessionStorage.getItem("isCoatingRunning");
                            if(isCoatingRunning != null && isCoatingRunning != "true"){
                                var coatingIterations = JSON.parse(sessionStorage.getItem("coatingIterations"));
                                var attributes = JSON.parse(sessionStorage.getItem("globalRelation")).attributes;
                                var $iterationCoutingLink = $('#udn-iterations-coating');
                                $iterationCoutingLink.html('');
                                for(var i in coatingIterations){
                                    var curRelation = {attributes: attributes, functionalDependencies: coatingIterations[i]};
                                    // Set objects for file generation.
                                    var blob, url, a, li;

                                    // Set MIME type and encoding.
                                    var fileType = "application/json;charset=utf-8;";
                                    var fileName = "coatingIterations"+(i+1)+".json";

                                    // Set data on blob.
                                    blob = new Blob( [ JSON.stringify(curRelation, null, 4) ], { type: fileType } );
                                    // Set view.
                                    if ( blob ) {
                                        // Read blob.
                                        url = window.URL.createObjectURL( blob );

                                        li = document.createElement( "li" );
                                        // Create link.
                                        a = document.createElement( "a" );
                                        a.innerHTML = fileName;
                                        // Set href on link.
                                        a.href = url;
                                        // Set file name on link.
                                        a.download = fileName;
                                        // Set link on DOM.
                                        li.appendChild( a );

                                        $iterationCoutingLink.append(li);

                                    } else {
                                        alert("Error creating file");
                                    }
                                }
                                console.log(coatingIterations);
                            }else{
                                alert("The operation has not finished");
                            }
                        }catch(err){
                            alert("The operation has not finished");
                        }
                    } else {
                        alert("Please download a current browser, make part of the new wave!");
                    }
                }else{
                    alert("Please create a new diagram or import it from a JSON file");
                }
            }
        },

        GenerateERUtils = {
                generateER : function(){

                }
        }
})

$(document).ready(function(){

    if(typeof(Storage) !== "undefined") {
        sessionStorage.removeItem("globalRelation");
        sessionStorage.removeItem("isCoatingRunning");
        sessionStorage.removeItem("coatingIterations");


        /**
         * ************************************************
         * TEST Todo: Revome this man!
         * ************************************************
         */


        //$.getJSON('src/js/test.json', function(data){
        //    var functionalDependencies = [];
        //    functionalDependencies.push(data.functionalDependencies);
        //    sessionStorage.setItem("coatingIterations", JSON.stringify(functionalDependencies, null, 4));
        //})
        //sessionStorage.setItem("isCoatingRunning", false);
        //

        /**
         * ************************************************
         * END TEST Todo: Revome this man!
         * ************************************************
         */

    } else {
        alert("Please download a current browser, make part of the new wave!");
    }

    //Click on Import JSON
    $(".udn-import-json").click(function(){
        ImportJSONUtil.importJSON();
    });

    $('.udn-iterations-update').click(function(){
        CoatingIterationsUtils.updateIterations();
    });

    //Click on Import JSON
    $(".udn-export-json").click(function(){
        ExportJSONUtil.exportCurrentJSON();
    });
    //Click on Import JSON
    $(".udn-generate-er").click(function(){
        GenerateERUtils.exportCurrentJSON();
    });


    /**
     * Add Listeners for Drag and Drop Action
     * Todo: mirar donde poner estos bien bien
     */
    // Setup the dnd listeners.
    var dropZone = document.getElementById('drop_zone');
    dropZone.addEventListener('dragover', ImportJSONUtil.handleDragOver, false);
    dropZone.addEventListener('drop', ImportJSONUtil.handleFileSelect, false);
    document.getElementById('JSONFileSelector').addEventListener('change', ImportJSONUtil.handleFileSelectSelector, false);





})
/**
 * Created by hernanlamprea on 3/10/15.
 */

var ThirdNormalForm = {}

$(function(){
    ThirdNormalForm = {

        /**
        * Calcula si una relacion se encuentra en 3ra Forma Normal si no se
        * en 3ra forma descompone la relacion en otras en la cuales si se encuentran en 3ra forma
        * @param {relacion}
        * @return {arrayDeRelaciones}
        */
        execute : function (relation){

            var functionalDependenciesLocal = relation.functionalDependencies.slice();
            var attributesLocal = relation.attributes.slice();
            var keysLocal = relation.keys.slice();
            var resultdependencies = [];

            /**
            * Verificamos si se encuentra en 3ra formal normal
            * Si no es asi descomponemos la relacion
            */
            if(checkThirdNormalForm(functionalDependenciesLocal,keysLocal)){
                return relation;
            }else{
                resultdependencies=decomposeInThirdNormalForm(functionalDependenciesLocal, attributesLocal)
            }

            return resultdependencies;

        }


        /**
        *   Verifica por cada una de las dependencias si el implicado pertenece a una llave
        */
        function checkThirdNormalForm(functionalDependencies,keys){

            var isThirdNormalForm = true;

            for(var i in functionalDependencies){
                if(GeneralUtils.binarySearch(keys,functionalDependencies[i].implied)>0){
                    isThirdNormalForm = false;
                    return isThirdNormalForm;
                }
            }

        }


        /**
        *
        */
        function decomposeInThirdNormalForm(functionalDependenciesLocal, attributesLocal){


        }

})
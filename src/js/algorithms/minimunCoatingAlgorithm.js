/**
 * Created by hernanlamprea on 26/09/15.
 */

var MinimunCoatingAlgorithm = {}

$(function(){

    MinimunCoatingAlgorithm = {

        minimunCoatingIterations : [],

        /**
         *
         * @param functionalDependencies
         * @returns {*}
         */
        execute : function (functionalDependencies){

            sessionStorage.setItem("isCoatingRunning", true);
            sessionStorage.removeItem("coatingIterations");

            var functionalDependenciesCoating;

            functionalDependenciesCoating =
                MinimunCoatingAlgorithm.deleteStrangeAttributes(functionalDependencies);

            functionalDependenciesCoating =
                MinimunCoatingAlgorithm.deleteRedundantDependencies(functionalDependenciesCoating);

            sessionStorage.setItem("coatingIterations", JSON.stringify(MinimunCoatingAlgorithm.minimunCoatingIterations, null, 4));

            sessionStorage.setItem("isCoatingRunning", false);

            return functionalDependenciesCoating;

        },
        /**
         *
         * @param functionalDependencies
         * @returns {Array}
         */
        deleteStrangeAttributes : function (functionalDependencies) {

            var funtionalDependenciesWithoutStranges=[];

            for (var i in functionalDependencies){

                var functionalDependenciesTemp = functionalDependencies.slice();
                functionalDependenciesTemp.splice(i,1);

                var dependencie =  $.extend( {}, functionalDependencies[i] );

                if(dependencie.descriptor.length>1){

                    var dependencieWithoutStranges = MinimunCoatingAlgorithm.strangeAttributes(functionalDependenciesTemp, dependencie);

                    if(!MinimunCoatingAlgorithm.searchDependencieInDependencies(functionalDependenciesTemp, dependencieWithoutStranges)){
                        funtionalDependenciesWithoutStranges.push(dependencieWithoutStranges);
                    }

                }else{
                    funtionalDependenciesWithoutStranges.push(dependencie);
                }

                MinimunCoatingAlgorithm.minimunCoatingIterations.push(funtionalDependenciesWithoutStranges);

            }

            return funtionalDependenciesWithoutStranges;
        },

        /**
         *
         * @param funtionalDependencies
         * @param dependencie
         */
        strangeAttributes : function (funtionalDependencies, dependencie){
            var dependencietemp = $.extend( {}, dependencie);

            for(var d in dependencie.descriptor){

                var descriptorTemp = dependencie.descriptor.slice();

                descriptorTemp.splice(d,1);

                var closure = ClosureAlgorithm.execute(funtionalDependencies, descriptorTemp);

                if(MinimunCoatingAlgorithm.searchAttributeClosure(closure,dependencie.implied)){
                    console.log(descriptorTemp + ' Tenia un atributo Estraño ' + dependencietemp.descriptor[d]);
                    dependencietemp.descriptor = descriptorTemp;
                    if(descriptorTemp.length>1){
                        dependencietemp = MinimunCoatingAlgorithm.strangeAttributes(funtionalDependencies, dependencietemp);
                    }
                }
            }

            return dependencietemp;

        },
        /**
         *
         * @param functionalDependenciesGlobal
         * @returns {Array}
         */
        deleteRedundantDependencies : function(functionalDependenciesGlobal) {
            // El implicado en este punto ya es unico
            var functionalDependenciesLocal = [];
            var functionalDependencies = functionalDependenciesGlobal.slice();

            for (var cont=0; cont<functionalDependencies.length; cont++) {
                var functionalDependencieActual = $.extend( {}, functionalDependencies[cont] );
                var functionalDependenciesTemp = functionalDependencies.slice();

                // Eliminar la Dependencia Actual
                functionalDependenciesTemp.splice(cont, 1);

                // debo importar algorithms.js
                var closure =ClosureAlgorithm.execute(functionalDependenciesTemp,functionalDependencieActual.descriptor)

                // Si el Implicado existe en el Cierre se elimina la Dependencia
                if (!MinimunCoatingAlgorithm.searchAttributeClosure(closure, functionalDependencieActual.implied)) {
                    functionalDependenciesLocal.push(functionalDependencieActual);
                }else{
                    functionalDependencies.splice(cont,1);
                    cont--;
                }

                MinimunCoatingAlgorithm.minimunCoatingIterations.push(functionalDependenciesLocal);

            }

            return functionalDependenciesLocal;

        },
        /**
         *
         * @param closure
         * @param attribute
         * @returns {boolean}
         */
        searchAttributeClosure : function (closure, attribute) {

            var exist = false;

            for (var i in closure) {
                if (closure[i] == attribute) {
                    exist = true;
                }
            }

            return exist;
        },
        /**
         *
         * @param functionalDependencies
         * @param dependencie
         * @returns {boolean}
         */
        searchDependencieInDependencies: function (functionalDependencies, dependencie){

            var exist = false;

            for(var i in functionalDependencies){
                if(GeneralUtils.areArraysEquals(functionalDependencies[i].descriptor,dependencie.descriptor) &&
                    GeneralUtils.areArraysEquals(functionalDependencies[i].implied, dependencie.implied)){
                    exist = true;
                }
            }

            return exist;

        }

    }
})





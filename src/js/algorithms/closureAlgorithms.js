/**
 * Created by diegoamaya on 26/09/15.
 */

var ClosureAlgorithm = {}

$(function(){
    ClosureAlgorithm = {

        /**
         * Calcula el cierre de un descriptor
         * este método no guarda en ningún lugar el compendio de los cierres, de esto se
         * encarga el cliente de ese metodo
         * @param {ArrayOfDependencies} functionalDependencies dependencias funcionales a ser usadas
         * @param {Array} descriptorToCalculate arreglo normal de descriptor a calcular cierre
         * @returns {Array} arreglo normal con los atributos que conforman el cierre
         */
        execute : function (functionalDependencies, descriptorToCalculate){
            //Se esperan elementos ordenados
            //sortArray(descriptorToCalculate);
            var closure = [];
            var isEmpty = false;
            //First assignation
            for(var i in descriptorToCalculate){
                closure.push(descriptorToCalculate[i]);
            }
            //First assignation combination
            var possibleCombinations = null;
            var combinationsAlreadyUsed = [];
            var maxSizeDependencies = FunctionalDependenciesUtils.getMaxSizeFromDescriptors(functionalDependencies);
            var tempClosure = [];
            while(!isEmpty){
                possibleCombinations = CombinationUtils.findCombinations(closure,maxSizeDependencies);
                var impliedGroup = [];
                possibleCombinations = CombinationUtils.extractNotUsedCombination(possibleCombinations, combinationsAlreadyUsed);
                for(var i in possibleCombinations){
                    combinationsAlreadyUsed.push(possibleCombinations[i])
                    tempClosure = FunctionalDependenciesUtils.searchForImpliedGroup(functionalDependencies,possibleCombinations[i]);
                    if(tempClosure != null && tempClosure.length >0){
                        for(var j in tempClosure){
                            impliedGroup.push(tempClosure[j]);
                        }
                    }
                }

                if(impliedGroup != null && impliedGroup.length > 0){
                    for(var i in impliedGroup){
                        var implied = impliedGroup[i]
                        for(var j in implied){
                            if(closure.indexOf(implied[j]) == -1)
                                closure.push(implied[j]);
                        }

                    }
                }else{
                    isEmpty = true
                }
            }
            closure.sort();
            return closure;
        }
    }
})

